/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclasess;

import javax.swing.JOptionPane;

/**
 *
 * @author marco
 */
public class jintCotizacion extends javax.swing.JInternalFrame {

    /**
     * Creates new form jintCotizacion
     */
    public jintCotizacion() {
        initComponents();
        this.resize(1241,732);
        this.deshabilitar();
    }
    
    public void deshabilitar(){
        
        this.txtDesAuto.setEnabled(false);
        this.txtNumCot.setEnabled(false);
        this.txtPagoMensual.setEnabled(false);
        this.txtPago.setEnabled(false);
        this.txtPorcentajePagoInicial.setEnabled(false);
        this.txtPrecio.setEnabled(false);
        this.txtTotal.setEnabled(false);
       //botones
       this.btnMostrar.setEnabled(!false);
       this.btnGuardar.setEnabled(!false);
    }
    
    public void habilitar(){
        this.txtDesAuto.setEnabled(!false);
        this.txtNumCot.setEnabled(!false);
        this.txtPorcentajePagoInicial.setEnabled(!false);
        this.txtPrecio.setEnabled(!false);
        this.btnGuardar.setEnabled(true);
    }
    
    public void limpiar(){
        this.txtDesAuto.setText("");
        this.txtNumCot.setText("");
        this.txtPago.setText("");
        this.txtPagoMensual.setText("");
        this.txtPrecio.setText("");
        this.txtTotal.setText("");
        this.txtPorcentajePagoInicial.setText("");
        
        //poner en la primera posicion al combo box
        this.cmbPlazos.setSelectedIndex(0);
        
        //poner cursor en el numero de cotizacion
        this.txtNumCot.requestFocus();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel7 = new javax.swing.JLabel();
        txtNumCot = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtDesAuto = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtPorcentajePagoInicial = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnNuevo = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtPago = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        txtPagoMensual = new javax.swing.JTextField();
        cmbPlazos = new javax.swing.JComboBox<>();

        setBackground(new java.awt.Color(204, 255, 255));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        getContentPane().setLayout(null);

        jLabel7.setText("Numero de Cotizacion:");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(80, 30, 170, 20);

        txtNumCot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumCotActionPerformed(evt);
            }
        });
        getContentPane().add(txtNumCot);
        txtNumCot.setBounds(260, 20, 84, 26);

        jLabel8.setText("Descripcion del automovil:");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(60, 60, 190, 20);

        txtDesAuto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDesAutoActionPerformed(evt);
            }
        });
        getContentPane().add(txtDesAuto);
        txtDesAuto.setBounds(260, 50, 80, 26);

        jLabel1.setText("Precio:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(80, 90, 49, 20);

        txtPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioActionPerformed(evt);
            }
        });
        getContentPane().add(txtPrecio);
        txtPrecio.setBounds(260, 80, 80, 26);

        jLabel2.setText("Porcentaje de pago Inicial:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(60, 120, 190, 20);
        getContentPane().add(txtPorcentajePagoInicial);
        txtPorcentajePagoInicial.setBounds(260, 110, 80, 26);

        jLabel3.setText("Plazos:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(70, 160, 70, 20);

        btnNuevo.setBackground(new java.awt.Color(153, 153, 255));
        btnNuevo.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(820, 40, 130, 70);

        btnCerrar.setBackground(new java.awt.Color(153, 153, 255));
        btnCerrar.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(500, 420, 130, 70);

        btnGuardar.setBackground(new java.awt.Color(153, 153, 255));
        btnGuardar.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(820, 120, 130, 70);

        btnCancelar.setBackground(new java.awt.Color(153, 153, 255));
        btnCancelar.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(300, 420, 130, 70);

        btnMostrar.setBackground(new java.awt.Color(153, 153, 255));
        btnMostrar.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(820, 200, 130, 70);

        btnLimpiar.setBackground(new java.awt.Color(153, 153, 255));
        btnLimpiar.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(100, 420, 130, 70);

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos de la Cotización", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jPanel2.setForeground(new java.awt.Color(204, 255, 204));

        jLabel4.setText("Pago Inicial:");

        jLabel5.setText("Total a Financiar:");

        jLabel6.setText("Pago Mensual:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtPagoMensual, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(90, 90, 90)
                        .addComponent(txtPago))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                        .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(223, 223, 223))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtPagoMensual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(49, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2);
        jPanel2.setBounds(130, 200, 530, 190);

        cmbPlazos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "12 meses", "24 meses", "36 meses", "48 meses", "60 meses" }));
        getContentPane().add(cmbPlazos);
        cmbPlazos.setBounds(260, 150, 100, 26);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtDesAutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDesAutoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDesAutoActionPerformed

    private void txtPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int opcion =0;
        
        opcion = JOptionPane.showConfirmDialog(this, "¿Deseas cerrar?","Cotizacion", JOptionPane.YES_NO_OPTION);
        
        if( opcion == JOptionPane.YES_OPTION){
            this.dispose();
        }
        
        
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        con = new Cotizacion(); //se construye el objeto
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        //hacer una validación 
        boolean exito = false;
        if (this.txtNumCot.getText().equals(""))exito = true;
        if (this.txtDesAuto.getText().equals(""))exito = true;
        if (this.txtPorcentajePagoInicial.getText().equals(""))exito = true;
        if (this.txtPrecio.getText().equals(""))exito = true;
        
        if (exito == true){
            //falto informacion 
            JOptionPane.showMessageDialog(this, "Falto capturar información");
            
        
        
        }
        else {
        //todo correcto
        con.setNumCotizacion(Integer.parseInt(this.txtNumCot.getText()));
        con.setDescripcionAuto(this.txtDesAuto.getText());
        int plazo =this.cmbPlazos.getSelectedIndex();
        switch (plazo){
                case 0:con.setPlazo(12);
                case 1:con.setPlazo(24);
                case 2:con.setPlazo(36);
                case 3:con.setPlazo(48);
                case 4:con.setPlazo(60);
                
                
                }
        con.setPorcentajePago(Float.parseFloat(this.txtPorcentajePagoInicial.getText()));
        con.setPrecio(Float.parseFloat(this.txtPrecio.getText()));
        
        JOptionPane.showMessageDialog(this, "Se guardo con exito la informacion");
        this.btnMostrar.setEnabled(true);
        
        }
           
        
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
        
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        //mostrar la informacion del objeto de la interface
        this.txtNumCot.setText(String.valueOf(con.getNumCotizacion()));
        this.txtDesAuto.setText(con.getDescripcionAuto());
        this.txtPorcentajePagoInicial.setText(String.valueOf(con.getPorcentajePago()));
        this.txtPrecio.setText(String.valueOf(con.getPrecio()));
        //plazos
       int plazo =con.getPlazo();
       
       switch(plazo){
        case 12:
        this.cmbPlazos.setSelectedIndex(0); break;
        case 24:
        this.cmbPlazos.setSelectedIndex(1); break;
        case 36:
        this.cmbPlazos.setSelectedIndex(2); break;
        case 48:
        this.cmbPlazos.setSelectedIndex(3); break;
        case 60:
        this.cmbPlazos.setSelectedIndex(4); break;
        }   
        
        //mostrar los resultados 
        this.txtPago.setText(String.valueOf(con.calcularPagoInicial()));
        this.txtPagoMensual.setText(String.valueOf(con.calcularPagoMenual()));
        this.txtTotal.setText(String.valueOf(con.calcularTotalFinanciar()));
        
        
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void txtNumCotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumCotActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumCotActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbPlazos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtDesAuto;
    private javax.swing.JTextField txtNumCot;
    private javax.swing.JTextField txtPago;
    private javax.swing.JTextField txtPagoMensual;
    private javax.swing.JTextField txtPorcentajePagoInicial;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
private Cotizacion con;
}
